package com.learning;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@SpringBootApplication
public class SpringBootExample {

	private static final Logger LOGGER = LoggerFactory.getLogger(SpringBootExample.class);
	
	public static void main(String[] args) 
	{
		LOGGER.info("*** Starting Spring Boot application ****");
		SpringApplication.run(SpringBootExample.class, args);
		startBatching();
		LOGGER.info("**** Initialization of service completed ****");
	}
	
   @Bean
   public Docket productApi() 
   {
      return new Docket(DocumentationType.SWAGGER_2).select()
         .apis(RequestHandlerSelectors.basePackage("com.learning.controller")).build();
   }

	public static void startBatching()
	{
		LOGGER.info("*** Entering startBatching  ****");
		TestClass t1 = new TestClass();
		t1.displayApplicationProperties();
		LOGGER.info("*** Exiting startBatching ****");
	}
	

}

package com.learning.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.learning.model.Course;

@RestController
public class CourseControllerV2 {
	
   private static Map<String, Course> courseRepo = new HashMap<>();
   
   static 
   {
	  Course course1 = new Course();
      course1.setCourseId("4KCJ001A");
      course1.setCourseName("Core Java - Basics1");
      courseRepo.put(course1.getCourseId(), course1);
      
      Course course2 = new Course();
      course2.setCourseId("4KCJ002A");
      course2.setCourseName("Core Java - Intermediate1");
      courseRepo.put(course2.getCourseId(), course2);
   }	
	
/*   @GetMapping("/index")
   public String index() 
   {
      return "index";
   }*/
   
   @GetMapping("/User")
   public ResponseEntity<Object> getUser(@RequestParam(name = "userName") String userName)
   {
      return new ResponseEntity<>("Request sent by "+ userName , HttpStatus.OK);
      
   }
   
   @GetMapping("/UserDetails")
   public ResponseEntity<Object> getUser(@RequestParam(name = "userName") String userName,
		   								 @RequestHeader Map<String, String> reqHeaderDetails)
   {
	  System.out.println("printing the header" + reqHeaderDetails);
      return new ResponseEntity<>("Request sent by "+ userName , HttpStatus.OK);
      
   }
   
   @GetMapping("/test")
   public ResponseEntity<Object> getRequestHeader(@RequestHeader String Authorization)
   {
	  System.out.println("printing header " + Authorization);
      return new ResponseEntity<>("Retreived details "+ Authorization , HttpStatus.OK);
      
   }
   
   //@RequestMapping(value = "/CoursesV2", method = RequestMethod.GET)
   @GetMapping("/CoursesV2")
   public ResponseEntity<Object> getCourses() 
   {
      return new ResponseEntity<>(courseRepo.values(), HttpStatus.OK);
   }
   
   //@RequestMapping(value = "/CoursesV2", method = RequestMethod.POST)
   @PostMapping("/CoursesV2")
   public ResponseEntity<Object> createCourse(@RequestBody Course Course) 
   {
	  courseRepo.put(Course.getCourseId(), Course);
      return new ResponseEntity<>("Course is created successfully", HttpStatus.CREATED);
   }   
   
   //@RequestMapping(value = "/CoursesV2/{id}", method = RequestMethod.PUT)
   @PutMapping("/CoursesV2/{id}")
   public ResponseEntity<Object> updateCourse(@PathVariable("id") String id, @RequestBody Course course) 
   { 
	  courseRepo.remove(id);
      course.setCourseId(id);
      courseRepo.put(id, course);
      return new ResponseEntity<>("Course is updated successsfully", HttpStatus.OK);
   }   
   
   //@RequestMapping(value = "/CoursesV2/{id}", method = RequestMethod.DELETE)
   @DeleteMapping("/CoursesV2/{id}")
   public ResponseEntity<Object> deleteCourse(@PathVariable("id") String id) 
   { 
	  courseRepo.remove(id);
      return new ResponseEntity<>("Course is deleted successsfully", HttpStatus.OK);
   }   
}

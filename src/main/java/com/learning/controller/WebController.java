package com.learning.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.learning.model.Course;


@Controller 
public class WebController 
{
   private static Map<String, Course> courseRepo = new HashMap<>();
	
   @GetMapping("/index")
   public String index() {
      return "index";
   }	
   
   @GetMapping("/input")
   public String input() 
   {
      return "input";
   }	   
   
   @PostMapping("/save")
   public ModelAndView save(@ModelAttribute Course course)  
   {    
	   ModelAndView modelAndView = new ModelAndView();    
	   modelAndView.setViewName("course-data");        
	   modelAndView.addObject("course", course);      

	   courseRepo.put(course.getCourseId(), course);
	   
	   return modelAndView;    
   }    
}


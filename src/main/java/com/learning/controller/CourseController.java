package com.learning.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.learning.model.Course;

@RestController
public class CourseController {
	
   private static Map<String, Course> courseRepo = new HashMap<>();
   
   static 
   {
	  Course course1 = new Course();
      course1.setCourseId("4KCJ001");
      course1.setCourseName("Core Java - Basics");
      courseRepo.put(course1.getCourseId(), course1);
      
      Course course2 = new Course();
      course2.setCourseId("4KCJ002");
      course2.setCourseName("Core Java - Intermediate");
      courseRepo.put(course2.getCourseId(), course2);
   }	
	
   @RequestMapping(value = "/Courses", method = RequestMethod.GET)
   public ResponseEntity<Object> getCourses() 
   {
      return new ResponseEntity<>(courseRepo.values(), HttpStatus.OK);
   }
   
   @RequestMapping(value = "/Courses", method = RequestMethod.POST)
   public ResponseEntity<Object> createCourse(@RequestBody Course Course) 
   {
	  courseRepo.put(Course.getCourseId(), Course);
      return new ResponseEntity<>("Course is created successfully", HttpStatus.CREATED);
   }   
   
   @RequestMapping(value = "/Courses/{id}", method = RequestMethod.PUT)
   public ResponseEntity<Object> updateCourse(@PathVariable("id") String id, @RequestBody Course course) 
   { 
	  courseRepo.remove(id);
      course.setCourseId(id);
      courseRepo.put(id, course);
      return new ResponseEntity<>("Course is updated successsfully", HttpStatus.OK);
   }   
   
   @RequestMapping(value = "/Courses/{id}", method = RequestMethod.DELETE)
   public ResponseEntity<Object> deleteCourse(@PathVariable("id") String id) 
   { 
	  courseRepo.remove(id);
      return new ResponseEntity<>("Course is deleted successsfully", HttpStatus.OK);
   }   
}
